package com.yuanxin.yuanxinkit.bean;

/**
 * Created by LIYAN on 2016/9/22.
 */
public class ShareMessage {

    private String Name;
    private String Title;
    private String ImageSource;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getImageSource() {
        return ImageSource;
    }

    public void setImageSource(String imageSource) {
        ImageSource = imageSource;
    }

    @Override
    public String toString() {
        return "ShareMessage{" +
                "Name='" + Name + '\'' +
                ", Title='" + Title + '\'' +
                ", ImageSource='" + ImageSource + '\'' +
                '}';
    }

    public ShareMessage(String name, String title, String imageSource) {
        Name = name;
        Title = title;
        ImageSource = imageSource;
    }

    public ShareMessage() {
        super();
    }
}
