package com.yuanxin.yuanxinkit.bean;

/**
 * Created by LIYAN on 2016/9/22.
 */
public class ShareList {

    private ShareListItem NativeMap;

    public ShareListItem getNativeMap() {
        return NativeMap;
    }

    public void setNativeMap(ShareListItem nativeMap) {
        NativeMap = nativeMap;
    }

    @Override
    public String toString() {
        return "ShareList{" +
                "NativeMap=" + NativeMap +
                '}';
    }

    public ShareList(ShareListItem nativeMap) {
        NativeMap = nativeMap;
    }

    public ShareList() {
        super();
    }
}
