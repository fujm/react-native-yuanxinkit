package com.yuanxin.yuanxinkit.bean;

/**
 * Created by LIYAN on 2016/9/22.
 */
public class ShareData {

    private SharePattern Layout;

    public SharePattern getLayout() {
        return Layout;
    }

    public void setLayout(SharePattern layout) {
        Layout = layout;
    }

    @Override
    public String toString() {
        return "ShareData{" +
                "Layout=" + Layout +
                '}';
    }

    public ShareData(SharePattern layout) {
        Layout = layout;
    }

    public ShareData() {
        super();
    }
}
