package com.yuanxin.yuanxinkit.bean;

/**
 * Created by LIYAN on 2016/11/25.
 */

public class Login {

    private String access_token;
    private String token_type;
    private int expires_in;
    private String refresh_token;
    private String expires_time;
    private String open_id;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getExpires_time() {
        return expires_time;
    }

    public void setExpires_time(String expires_time) {
        this.expires_time = expires_time;
    }

    public String getOpen_id() {
        return open_id;
    }

    public void setOpen_id(String open_id) {
        this.open_id = open_id;
    }

    public Login(String access_token, String token_type, int expires_in, String refresh_token, String expires_time, String open_id) {
        this.access_token = access_token;
        this.token_type = token_type;
        this.expires_in = expires_in;
        this.refresh_token = refresh_token;
        this.expires_time = expires_time;
        this.open_id = open_id;
    }

    public Login() {
        super();
    }

    @Override
    public String toString() {
        return "Login{" +
                "access_token='" + access_token + '\'' +
                ", token_type='" + token_type + '\'' +
                ", expires_in=" + expires_in +
                ", refresh_token='" + refresh_token + '\'' +
                ", expires_time='" + expires_time + '\'' +
                ", open_id='" + open_id + '\'' +
                '}';
    }
}
