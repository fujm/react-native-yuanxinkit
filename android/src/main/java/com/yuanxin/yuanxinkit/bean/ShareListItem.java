package com.yuanxin.yuanxinkit.bean;

import java.util.List;

/**
 * Created by LIYAN on 2016/9/22.
 */
public class ShareListItem {

    private List<ShareMessage> Items;

    public List<ShareMessage> getItems() {
        return Items;
    }

    public void setItems(List<ShareMessage> items) {
        Items = items;
    }

    @Override
    public String toString() {
        return "ShareListItem{" +
                "Items=" + Items +
                '}';
    }

    public ShareListItem(List<ShareMessage> items) {
        Items = items;
    }

    public ShareListItem() {
        super();
    }
}
