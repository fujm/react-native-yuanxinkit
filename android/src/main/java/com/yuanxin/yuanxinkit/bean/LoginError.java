package com.yuanxin.yuanxinkit.bean;

import java.io.Serializable;

/**
 * Created by LIYAN on 2017/3/13.
 */

public class LoginError implements Serializable{

    private int ErrorCode;
    private String ErrorMsg;

    public int getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(int errorCode) {
        ErrorCode = errorCode;
    }

    public String getErrorMsg() {
        return ErrorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        ErrorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "LoginError{" +
                "ErrorCode=" + ErrorCode +
                ", ErrorMsg='" + ErrorMsg + '\'' +
                '}';
    }

    public LoginError() {
        super();
    }
}
