package com.yuanxin.yuanxinkit.bean;

/**
 * Created by LIYAN on 2016/11/30.
 */

public class IMLogin {

    private String imConnectionId;
    private int userId;
    private String userName;
    private String uuid;
    private String photo;

    public String getImConnectionId() {
        return imConnectionId;
    }

    public void setImConnectionId(String imConnectionId) {
        this.imConnectionId = imConnectionId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public IMLogin(String imConnectionId, int userId) {
        this.imConnectionId = imConnectionId;
        this.userId = userId;
    }

    public IMLogin() {
        super();
    }
}
