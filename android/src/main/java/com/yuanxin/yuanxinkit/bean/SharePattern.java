package com.yuanxin.yuanxinkit.bean;

/**
 * Created by LIYAN on 2016/9/22.
 */
public class SharePattern {

    private int maxLine;
    private int maxColume;
    private int scrollDirection;
    private int contentModel;
    private int cornerRadius;
    private int theLengthToSide;

    public int getMaxLine() {
        return maxLine;
    }

    public void setMaxLine(int maxLine) {
        this.maxLine = maxLine;
    }

    public int getMaxColume() {
        return maxColume;
    }

    public void setMaxColume(int maxColume) {
        this.maxColume = maxColume;
    }

    public int getScrollDirection() {
        return scrollDirection;
    }

    public void setScrollDirection(int scrollDirection) {
        this.scrollDirection = scrollDirection;
    }

    public int getContentModel() {
        return contentModel;
    }

    public void setContentModel(int contentModel) {
        this.contentModel = contentModel;
    }

    public int getCornerRadius() {
        return cornerRadius;
    }

    public void setCornerRadius(int cornerRadius) {
        this.cornerRadius = cornerRadius;
    }

    public int getTheLengthToSide() {
        return theLengthToSide;
    }

    public void setTheLengthToSide(int theLengthToSide) {
        this.theLengthToSide = theLengthToSide;
    }

    @Override
    public String toString() {
        return "SharePattern{" +
                "maxLine=" + maxLine +
                ", maxColume=" + maxColume +
                ", scrollDirection=" + scrollDirection +
                ", contentModel=" + contentModel +
                ", cornerRadius=" + cornerRadius +
                ", theLengthToSide=" + theLengthToSide +
                '}';
    }

    public SharePattern(int maxLine, int maxColume, int scrollDirection, int contentModel, int cornerRadius, int theLengthToSide) {
        this.maxLine = maxLine;
        this.maxColume = maxColume;
        this.scrollDirection = scrollDirection;
        this.contentModel = contentModel;
        this.cornerRadius = cornerRadius;
        this.theLengthToSide = theLengthToSide;
    }

    public SharePattern() {
        super();
    }
}
