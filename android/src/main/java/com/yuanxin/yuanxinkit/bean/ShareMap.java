package com.yuanxin.yuanxinkit.bean;

/**
 * Created by LIYAN on 2016/9/22.
 */
public class ShareMap {

    private ShareData NativeMap;

    public ShareData getNativeMap() {
        return NativeMap;
    }

    public void setNativeMap(ShareData nativeMap) {
        NativeMap = nativeMap;
    }

    @Override
    public String toString() {
        return "ShareMap{" +
                "NativeMap=" + NativeMap +
                '}';
    }

    public ShareMap(ShareData nativeMap) {
        NativeMap = nativeMap;
    }

    public ShareMap() {
        super();
    }
}
