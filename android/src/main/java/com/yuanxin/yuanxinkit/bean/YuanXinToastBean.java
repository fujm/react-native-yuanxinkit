package com.yuanxin.yuanxinkit.bean;

/**
 * Created by lemon on 2017/4/19.
 */

public class YuanXinToastBean {

    private int textAlignment;

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public float getTitleFontSize() {
        return titleFontSize;
    }

    public void setTitleFontSize(float titleFontSize) {
        this.titleFontSize = titleFontSize;
    }

    public String getTitleFontWeight() {
        return titleFontWeight;
    }

    public void setTitleFontWeight(String titleFontWeight) {
        this.titleFontWeight = titleFontWeight;
    }

    public int getDesTextAlignment() {
        return desTextAlignment;
    }

    public void setDesTextAlignment(int desTextAlignment) {
        this.desTextAlignment = desTextAlignment;
    }

    public String getDesTextColor() {
        return desTextColor;
    }

    public void setDesTextColor(String desTextColor) {
        this.desTextColor = desTextColor;
    }

    public float getDesFontSize() {
        return desFontSize;
    }

    public void setDesFontSize(float desFontSize) {
        this.desFontSize = desFontSize;
    }

    public String getDesFontWeight() {
        return desFontWeight;
    }

    public void setDesFontWeight(String desFontWeight) {
        this.desFontWeight = desFontWeight;
    }

    public int getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

    public int getPadding() {
        return padding;
    }

    public void setPadding(int padding) {
        this.padding = padding;
    }

    public boolean getForceUserInteraction() {
        return forceUserInteraction;
    }

    public void setForceUserInteraction(boolean forceUserInteraction) {
        this.forceUserInteraction = forceUserInteraction;
    }

    public int getTextAlignment() {
        return textAlignment;
    }

    public void setTextAlignment(int textAlignment) {
        this.textAlignment = textAlignment;
    }

    private String textColor;
    private float titleFontSize;
    private String titleFontWeight;
    private int desTextAlignment;
    private String desTextColor;
    private float desFontSize;
    private String desFontWeight;
    private int timeInterval;
    private int padding;
    private boolean forceUserInteraction;
    private String backgroundColor;
    private String title;
    private String style;
    private boolean showActivityIndicator;
    private int imageAlignment;

    public int getActivityIndicatorAlignment() {
        return activityIndicatorAlignment;
    }

    public void setActivityIndicatorAlignment(int activityIndicatorAlignment) {
        this.activityIndicatorAlignment = activityIndicatorAlignment;
    }

    private int activityIndicatorAlignment;

    public int getImageAlignment() {
        return imageAlignment;
    }

    public void setImageAlignment(int imageAlignment) {
        this.imageAlignment = imageAlignment;
    }

    private String imageUrl;

    private String tagID;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTagID() {
        return tagID;
    }

    public void setTagID(String tagID) {
        this.tagID = tagID;
    }

    public boolean getShowActivityIndicator() {
        return showActivityIndicator;
    }

    public void setShowActivityIndicator(boolean showActivityIndicator) {
        this.showActivityIndicator = showActivityIndicator;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String description;
}
