package com.yuanxin.yuanxinkit;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;

import cn.bertsir.zbar.utils.QRUtils;

public class ImageKitModule extends ReactContextBaseJavaModule {


    public ImageKitModule(ReactApplicationContext reactContext) {
        super(reactContext);

    }

    @Override
    public String getName() {
        return "ImageKit";
    }

    @ReactMethod
    public void detectorQrCode(ReadableMap pathInfo, Promise promise) {
        Bitmap scanBitmap;
        if (pathInfo == null) {
            promise.reject("-1", "args is null!");
            return;
        }
        String filePath;
        if (!pathInfo.hasKey("filePath")) {
            promise.reject("-1", "filePath is null!");
            return;
        }
        filePath = pathInfo.getString("filePath");


        String result = null;


        try {
            result = QRUtils.getInstance().decodeQRcode(filePath);
            if (result == null || "".equals(result)) {
                for (float c = 0.5f; c < 2; c += 0.2) {
                    Bitmap bitmap = contrast(filePath, c);
                    result = QRUtils.getInstance().decodeQRcode(bitmap);
                    bitmap.recycle();
                    if (result != null && !"".equals(result))
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result == null || "".equals(result)) {
            promise.reject("-1", "图片二维码识别失败");
        } else {
            WritableMap map = Arguments.createMap();
            map.putString("data", result);
            promise.resolve(map);
        }
    }

    public Bitmap contrast(String filePath, float contrast) {

        Bitmap srcBitmap = BitmapFactory.decodeFile(filePath, new BitmapFactory.Options());

        int imgHeight = srcBitmap.getHeight();
        int imgWidth = srcBitmap.getWidth();

        Bitmap bmp = Bitmap.createBitmap(imgWidth, imgHeight,
                Bitmap.Config.ARGB_8888);
        ColorMatrix cMatrix = new ColorMatrix();
        cMatrix.set(new float[]{contrast, 0, 0, 0, 0, 0,
                contrast, 0, 0, 0,// 改变对比度
                0, 0, contrast, 0, 0, 0, 0, 0, 1, 0});

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Canvas canvas = new Canvas(bmp);
        // 在Canvas上绘制一个已经存在的Bitmap。这样，dstBitmap就和srcBitmap一摸一样了
        canvas.drawBitmap(srcBitmap, 0, 0, paint);

        return bmp;
    }

}
