package com.yuanxin.yuanxinkit;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.yuanxin.yuanxinkit.bean.YuanXinToastBean;
import com.yuanxin.yuanxinkit.utils.FloatViewUtil;
import com.yuanxin.yuanxinkit.utils.NotificationUtil;


public class YuanXinToastManager extends ReactContextBaseJavaModule {
    public YuanXinToastManager(ReactApplicationContext reactContext) {
        super(reactContext);
        NotificationUtil.init(reactContext);
        FloatViewUtil.init(reactContext);
    }

    /**
     * @return the name of this module. This will be the name used to {@code require()} this module
     * from javascript.
     */
    @Override
    public String getName() {
        return "YuanXinToastManager";
    }

    /**
     * @param map
     * @param promise
     */
    @ReactMethod
    public void showNavigationBar(ReadableMap map, final Promise promise) {
        YuanXinToastBean yuanXinToastBean = this.getToastBean(map);
        try {
            FloatViewUtil.showFloatView(yuanXinToastBean, promise);
        } catch (Exception ex) {
            promise.reject("error", ex.getMessage());
        }
    }

    @ReactMethod
    public void showNotifications(ReadableMap map, final Promise promise) {
        YuanXinToastBean yuanXinToastBean = this.getToastBean(map);
        try {
            FloatViewUtil.showFloatView(yuanXinToastBean, promise);
        } catch (Exception ex) {
            promise.reject("error", ex.getMessage());
        }
    }

    @ReactMethod
    public void showStatusBar(ReadableMap map, final Promise promise) {
        try {
            String tagID = "";
            String title = "";
            String message = "";
            int timeInterval = -1;
            if (map.hasKey("tagID"))
                tagID = map.getString("tagID");
            if (map.hasKey("title"))
                title = map.getString("title");
            if (map.hasKey("message"))
                message = map.getString("message");
            if (map.hasKey("timeInterval"))
                timeInterval = (int) (map.getDouble("timeInterval") * 1000);
            tagID = NotificationUtil.show(tagID, title, message, timeInterval);
            promise.resolve(tagID);
        } catch (Exception ex) {
            promise.reject("error", ex.getMessage());
        }
    }

    YuanXinToastBean getToastBean(ReadableMap map) {
        YuanXinToastBean bean = new YuanXinToastBean();
        ReadableMap textFont;
        if (map.hasKey("textAlignment"))
            bean.setTextAlignment(map.getInt("textAlignment"));
        if (map.hasKey("textColor"))
            bean.setTextColor(map.getString("textColor"));
        if (map.hasKey("textFont")) {
            textFont = map.getMap("textFont");
            if (textFont.hasKey("fontSize"))
                bean.setTitleFontSize((float) textFont.getDouble("fontSize"));
            if (textFont.hasKey("fontWeight"))
                bean.setTitleFontWeight(textFont.getString("fontWeight"));
        }
        if (map.hasKey("desTextAlignment"))
            bean.setDesTextAlignment(map.getInt("desTextAlignment"));
        if (map.hasKey("desTextColor"))
            bean.setDesTextColor(map.getString("desTextColor"));
        ReadableMap desFont;
        if (map.hasKey("desTextFont")) {
            desFont = map.getMap("textFont");
            if (desFont.hasKey("fontSize"))
                bean.setDesFontSize((float) desFont.getDouble("fontSize"));
            if (desFont.hasKey("fontWeight"))
                bean.setDesFontWeight(desFont.getString("fontWeight"));
        }
        if (map.hasKey("timeInterval"))
            bean.setTimeInterval((int) (map.getDouble("timeInterval") * 1000));
        if (map.hasKey("padding"))
            bean.setPadding(map.getInt("padding"));
        if (map.hasKey("forceUserInteraction"))
            bean.setForceUserInteraction(map.getBoolean("forceUserInteraction"));
        if (map.hasKey("backgroundColor"))
            bean.setBackgroundColor(map.getString("backgroundColor"));
        if (map.hasKey("title"))
            bean.setTitle(map.getString("title"));
        if (map.hasKey("description"))
            bean.setDescription(map.getString("description"));
        if (map.hasKey("style"))
            bean.setStyle(map.getString("style"));
        if (map.hasKey("showActivityIndicator"))
            bean.setShowActivityIndicator(map.getBoolean("showActivityIndicator"));
        if (map.hasKey("imageAlignment"))
            bean.setImageAlignment(map.getInt("imageAlignment"));
        if (map.hasKey("imageUrl"))
            bean.setImageUrl(map.getString("imageUrl"));
        if (map.hasKey("tagID"))
            bean.setTagID(map.getString("tagID"));
        if (map.hasKey("activityIndicatorAlignment"))
            bean.setActivityIndicatorAlignment(map.getInt("activityIndicatorAlignment"));
        if (map.hasKey("imageSource")) {
            ReadableMap imageMap = map.getMap("imageSource");
            bean.setImageUrl(imageMap.getString("uri"));
        }
        return bean;
    }
}
