package com.yuanxin.yuanxinkit;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.WritableMap;
import com.yuanxin.yuanxinkit.R;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.google.gson.Gson;
import com.yuanxin.yuanxinkit.adapter.BouncedAdapter;
import com.yuanxin.yuanxinkit.bean.ShareList;
import com.yuanxin.yuanxinkit.bean.ShareMap;
import com.yuanxin.yuanxinkit.bean.ShareMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LIYAN on 2016/9/23.
 */
public class BouncedModule extends ReactContextBaseJavaModule {

    private static String TAG=BouncedModule.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private List<String> mDatas;
    private BouncedAdapter mAdapter;
    private int PoP_Height;
    private PopupWindow popupWindow;

    public BouncedModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "YuanXinPickerManager";
    }

    @ReactMethod
    public void showShareView(final ReadableMap map, final Promise promise){

       // Log.e(TAG,"showShareView-map-"+map);

        Gson gson = new Gson();

        ShareMap shareMap = gson.fromJson(String.valueOf(map), ShareMap.class);
        int MaxLine = shareMap.getNativeMap().getLayout().getMaxLine();  //行
        int MaxColume = shareMap.getNativeMap().getLayout().getMaxColume();  //列
        int ScrollDirection = shareMap.getNativeMap().getLayout().getScrollDirection();  //横向 1   竖向 0
        int ContentModel = shareMap.getNativeMap().getLayout().getContentModel(); //位置   中 1  底部 0  上 2
        int CornerRadius = shareMap.getNativeMap().getLayout().getCornerRadius(); //圆角
        int TheLengthToSide = shareMap.getNativeMap().getLayout().getTheLengthToSide(); //边距
       // Log.e(TAG,"MaxLine---"+MaxLine+"---MaxColume---"+MaxColume+"---ScrollDirection---"+ScrollDirection
//                +"---ContentModel---"+ContentModel+"---CornerRadius---"+CornerRadius+"---TheLengthToSide---"+TheLengthToSide);

        ShareList shareList = gson.fromJson(String.valueOf(map), ShareList.class);
        final List<ShareMessage> listItems = shareList.getNativeMap().getItems();
        String Name = null;
        String Title = null;
        String ImageSource = null;
        for (int i = 0; i < listItems.size(); i++){
            Name = listItems.get(i).getName();
            Title = listItems.get(i).getTitle();
            ImageSource = listItems.get(i).getImageSource();
           // Log.e(TAG,"Name---"+Name);
           // Log.e(TAG,"Title---"+Title);
           // Log.e(TAG,"ImageSource---"+ImageSource);
        }

        View contentView = LayoutInflater.from(getCurrentActivity()).inflate(
                R.layout.share_recyclerview, null);

        mDatas = new ArrayList<String>();
        for (int i = 'A'; i < 'z'; i++)
        {
            mDatas.add("" + (char) i);
        }

        mRecyclerView = (RecyclerView) contentView.findViewById(R.id.id_recyclerview);
        mAdapter = new BouncedAdapter(getCurrentActivity(), listItems);

        if(ScrollDirection == 0){
            //竖
            if(listItems.size() <= 4){
                PoP_Height = 100;
            } else {
                if(MaxLine == 1){
                    PoP_Height = 100;
                } else {
                    PoP_Height = 200;
                }
            }
            mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(MaxColume,
                    StaggeredGridLayoutManager.VERTICAL));
        } else if(ScrollDirection == 1){
            //横
//            GridLayoutManager gridLayoutManager = new GridLayoutManager(getCurrentActivity(),2);
//            gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//            mRecyclerView.setLayoutManager(gridLayoutManager);
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT,100);
//            mRecyclerView.setLayoutParams(params);
            PoP_Height = 100;
            MaxColume = 1;

            if(listItems.size() > 4){
                if(MaxLine > 1){
                    MaxColume = 2;
                    PoP_Height = 200;
                }
            }
		    mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(MaxColume,
				    StaggeredGridLayoutManager.HORIZONTAL));
        } else {
           // Log.e(TAG,"传入参数有误---ScrollDirection---"+ScrollDirection);
        }

        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickLitener(new BouncedAdapter.OnItemClickLitener()
        {
            @Override
            public void onItemClick(View view, int position)
            {
//                Toast.makeText(getCurrentActivity(), position + " click",
//                        Toast.LENGTH_SHORT).show();
//                callback.invoke(position);
                String name = listItems.get(position).getName();
                WritableMap writableMap = Arguments.createMap();
                writableMap.putString("Name",name);
                promise.resolve(writableMap);
                popupWindow.dismiss();
            }

            @Override
            public void onItemLongClick(View view, int position)
            {
                Toast.makeText(getCurrentActivity(), position + " long click",
                        Toast.LENGTH_SHORT).show();
            }
        });

        popupWindow = new PopupWindow(getCurrentActivity());
        popupWindow.setWidth(ActionBar.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(Dp2Px(getCurrentActivity(),PoP_Height));
        popupWindow.setContentView(contentView);
        popupWindow.setTouchable(true);
//        backgroundAlpha(0.5f);

        popupWindow.setBackgroundDrawable(ContextCompat.getDrawable(getCurrentActivity(),R.drawable.cornerradius));

        popupWindow.setOutsideTouchable(true);

        popupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

//        popupWindow.setOnDismissListener(new poponDismissListener());

        if(ContentModel == 0){
            popupWindow.showAtLocation(new View(getCurrentActivity()), Gravity.BOTTOM,0,0);
        } else if(ContentModel == 1){
            popupWindow.showAtLocation(new View(getCurrentActivity()), Gravity.CENTER,0,0);
        } else if(ContentModel == 2){
            popupWindow.showAtLocation(new View(getCurrentActivity()), Gravity.TOP,0,0);
        } else {
           // Log.e(TAG,"传入参数有误---ContentModel---"+ScrollDirection);
        }

    }

    public void backgroundAlpha(float bgAlpha)
    {
        WindowManager.LayoutParams lp = getCurrentActivity().getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getCurrentActivity().getWindow().setAttributes(lp);
    }

    class poponDismissListener implements PopupWindow.OnDismissListener{
        @Override
        public void onDismiss() {
            backgroundAlpha(1f);
        }
    }
    public int Dp2Px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

}
