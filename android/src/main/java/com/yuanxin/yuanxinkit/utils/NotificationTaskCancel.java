package com.yuanxin.yuanxinkit.utils;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

/**
 * Created by lemon on 2017/4/10.
 */

public class NotificationTaskCancel extends AsyncTask<Void, Void, Void> {
    private final WeakReference<Integer> weakReferenceTime;
    private final WeakReference<String> weakReferenceTagID;

    public NotificationTaskCancel(int times, String tagID) {
        this.weakReferenceTime = new WeakReference<Integer>(times);
        this.weakReferenceTagID = new WeakReference<String>(tagID);
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Void doInBackground(Void... params) {
        try {
            if(this.weakReferenceTime.get()!=null)
                Thread.sleep(this.weakReferenceTime.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param aVoid The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        String tagID = this.weakReferenceTagID.get();
        if (tagID != null)
            NotificationUtil.cancelNotification(tagID);
    }
}