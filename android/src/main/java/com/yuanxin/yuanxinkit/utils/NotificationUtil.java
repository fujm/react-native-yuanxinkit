package com.yuanxin.yuanxinkit.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.yuanxin.yuanxinkit.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lemon on 2017/4/7.
 */


public class NotificationUtil {
    static NotificationManager notificationManager;
    public static final String NOTIFICATION_ONCLICK = "100";
    static Map<String, Integer> notificationMap;
    static int notificationID = 0;
    static Context mContext;

    public static void init(Context context) {
        mContext = context;
    }

    static void getNotificationManager() {
        if (notificationMap == null)
            notificationMap = new HashMap<String, Integer>();
        if (notificationManager == null) {
            notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        }
    }

    private static BroadcastReceiver receiver_onclick = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(NOTIFICATION_ONCLICK)) {
                //System.out.println(intent.getExtras().getString("id"));
            }
        }
    };

    public static String show(String tagID, String title, String textContent, int timeInterval) {
        getNotificationManager();
        if (tagID.length() == 0)
            tagID = java.util.UUID.randomUUID().toString().replace("-", "");
        if (notificationMap.containsKey(tagID))
            return "";
        else {
            notificationID++;
            notificationMap.put(tagID, notificationID);
            showNotification(title, textContent, tagID, timeInterval);
            return tagID;
        }
    }

    static void showNotification(String title, String textContent, String tagID, int timeInterval) {
        Bundle bundle = new Bundle();
        bundle.putString("id", tagID);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
        builder.setTicker(title);
        builder.setContentTitle(title);
        builder.setContentText(textContent);
        builder.setSubText(textContent);
        builder.setNumber(2);
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.drawable.yuanxin_icon);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(NOTIFICATION_ONCLICK);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(receiver_onclick, intentFilter);
        Intent intent = new Intent(NOTIFICATION_ONCLICK);
        intent.putExtras(bundle);
        PendingIntent pendIntent_click = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendIntent_click);
        builder.setDefaults(NotificationCompat.DEFAULT_ALL);
        Notification notification = builder.build();
        notificationManager.notify(notificationID, notification);
        if (timeInterval != -1) {
            NotificationTaskCancel notificationTaskCancel = new NotificationTaskCancel(timeInterval, tagID);
            notificationTaskCancel.execute();
        }
    }

    public static void cancelNotification(String tagID) {
        if (notificationMap.containsKey(tagID)) {
            notificationManager.cancel(notificationMap.get(tagID));
            notificationMap.remove(tagID);
        }
    }

}
