# 远薪Kit组件

## 总体介绍

 远薪Kit组件源码路径为http://yxsource.sinooceanland.net/platform/react-native-yuanxinkit

 该组件提供符合远薪登录，退出登录……业务服务，同时也提供一些比较常用的UI组件，例如分享弹框，消息提示…………

## 使用介绍

### npm 安装

1.在项目根目录的package.js里添加 "react-native-yuanxinkit": "git+http://yxsource.sinooceanland.net/platform/react-native-yuanxinkit.git"

2.执行 npm install

### IOS

配置 

1.在项目中选中Libraries文件夹，右键选择add File to "你的工程"，在node_modules文件夹下找到react-native-yuanxinkit/ios/YuanXinKit.xcodeproj，添加到项目中

2.选中项目在targets中的General选项中找到Linked Frameworks and Libraries并添加YuanXinKit.a

3.选中YuanXinCamera.xcodeproj项目在targets中的Build Settings选卡中找到Header Search Paths中添加$(SRCROOT)/../node_modules/react-native-yuanxinkit/ios/YuanXinKit/

4.找到项目中左侧YuanXinKit.xcodeproj->YuanXinKit->YuanXinKitResource.bundle，拖拽到名称下

不用选Copy items if needed

### android

1.在setting.gradle

```
include ':react-native-yuanxinkit'
project(':react-native-yuanxinkit').projectDir = new File(settingsDir, '../node_modules/react-native-yuanxinkit/android')
```

2.在app的build.gradle添加

```
compile project(':react-native-yuanxinkit')
```

3.在MainApplication添加

```
new BouncedPackage()
new InfoPackage()
new AccountPackage()
```

### 使用方式

Pickers 分享组件

```
import { Pickers } from 'react-native-yuanxinkit';

Pickers.sharePicker({}).then(selectItemName => {
   console.log(selectItemName);
}).catch(ex => {
   console.log(ex);
});
```

ToastManager 消息提示与状态提示功能

```
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableHighlight,
    Text,
    Image,
    ScrollView
} from 'react-native';

import BaseContainer from './BaseContainer';
import {
    Actions
} from 'react-native-router-flux';

import {
    ToastManager
} from 'react-native-yuanxinkit'

import image_share_qq from '../../images/ShareImages/share_qq_on.png';

export default class MessageBarContainer extends BaseContainer {

    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

/**
 * 状态栏提醒
 */
    onShowStatusBar(options: Object) {
        ToastManager.showStatusBar(options).then(result => {
            console.log(result);
        }).catch(er => {
            console.log(er);
        });
    }

/**
 * 自定议导航条提醒
 */
    onShowNavigationBar(options: Object) {
        ToastManager.showNavigationBar(options).then(result => {
            console.log(result);
        }).catch(er => {
            console.log(er);
        });
    }

/**
 * 错误消息提醒
 */
    onShowErrorNotification(options: Object) {
        ToastManager.showErrorNotification(options).then(result => {
            console.log(result);
        }).catch(er => {
            console.log(er);
        });
    }

/**
 * 成功消息提醒
 */
    onShowSuccessNotification(options: Object) {
        ToastManager.showSuccessNotification(options).then(result => {
            console.log(result);
        }).catch(er => {
            console.log(er);
        });
    }

/**
 * 警告信息提醒
 */
    onShowWarningNotification(options: Object) {
        ToastManager.showWarningNotification(options).then(result => {
            console.log(result);
        }).catch(er => {
            console.log(er);
        });
    }
/**
 * 普通消息提醒
 */
    onShowInfoNotification(options: Object) {
        ToastManager.showInfoNotification(options).then(result => {
            console.log(result);
        }).catch(er => {
            console.log(er);
        });
    }

    render() {
        return (
            <View style={[styles.container, this.props.style]} >
                {this.defaultRenderNavigationBar()}
                <ScrollView style={[styles.container]}>
                    <TouchableHighlight underlayColor={'transparent'} style={styles.button}
                        onPress={() => this.onShowStatusBar({ message: '任务栏提醒' })}>
                        <Text allowFontScaling={false}
                        >显示提醒</Text>
                    </TouchableHighlight>

                    <TouchableHighlight underlayColor={'transparent'} style={styles.button}
                        onPress={() => this.onShowNavigationBar({ title: '测试Title', description: 'description' })}>
                        <Text allowFontScaling={false}
                        >导航栏提醒</Text>
                    </TouchableHighlight>

                    <TouchableHighlight underlayColor={'transparent'} style={styles.button}
                        onPress={() => this.onShowNavigationBar({ title: '测试Title', description: 'description', showActivityIndicator: true })}>
                        <Text allowFontScaling={false}
                        >显示进度</Text>
                    </TouchableHighlight>

                    <TouchableHighlight underlayColor={'transparent'} style={styles.button}
                        onPress={() => this.onShowNavigationBar({ title: '测试Title', description: 'description', imageSource: image_share_qq })}>
                        <Text allowFontScaling={false}
                        >Local Resources image</Text>
                    </TouchableHighlight>

                    <TouchableHighlight underlayColor={'transparent'} style={styles.button}
                        onPress={() => this.onShowNavigationBar({ title: '测试Title', description: 'description', imageSource: { uri: 'http://yuanxinapp.oss-cn-beijing.aliyuncs.com/BusinessPlatform/b55fd169dcdf983eb3fc7ec0731bd291.png' } })}>
                        <Text allowFontScaling={false}
                        >Networking image</Text>
                    </TouchableHighlight>

                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={styles.button}
                        onPress={() => this.onShowNavigationBar({ title: '测试Title', description: 'description', forceUserInteraction: true })}>
                        <Text allowFontScaling={false}>点击消失提醒</Text>
                    </TouchableHighlight>

                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={styles.button}
                        onPress={() => this.onShowSuccessNotification({ title: 'Success Title', description: 'Success Description' })}>
                        <Text allowFontScaling={false}>Success Message</Text>
                    </TouchableHighlight>

                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={styles.button}
                        onPress={() => this.onShowErrorNotification({ title: 'Error Title', description: 'Error Description' })}>
                        <Text allowFontScaling={false}>Error Message</Text>
                    </TouchableHighlight>

                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={styles.button}
                        onPress={() => this.onShowWarningNotification({ title: 'Warning Title', description: 'Warning Description' })}>
                        <Text allowFontScaling={false}>Warning Message</Text>
                    </TouchableHighlight>

                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={styles.button}
                        onPress={() => this.onShowInfoNotification({ title: 'Info Title', description: 'Info Description' })}>
                        <Text allowFontScaling={false}>Info Message</Text>
                    </TouchableHighlight>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    button: {
        flex: 1,
        marginBottom: 10,
        height: 44,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#EEEEEE'
    }
});
```

