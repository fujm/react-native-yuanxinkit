Pod::Spec.new do |s|
  s.name             = "react-native-yuanxinkit"
  s.version          = "2.2.1"
  s.summary          = "A project committed to make file acess and data transfer easier, effiecient for React Native developers."
  s.requires_arc = true
  s.license      = 'MIT'
  s.homepage     = 'n/a'
  s.authors      = { "wkh237" => "xeiyan@gmail.com" }
  s.source       = { :git => "http://alm-tfs.sinooceangroup.com:8080/tfs/YXPlatform/_git/react-native-yuanxinkit", :tag => 'v2.2.1'}
  s.source_files = 'ios/**/*.{h,m}'
  s.platform     = :ios, "9.0"
  s.dependency 'React-Core'
  s.dependency 'react-native-yuanxinnetworking'
end
