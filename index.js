
import React, {
  Component,
  PropTypes
} from 'react';

import _YuanXinPicker from './js/YuanXinPicker';
import _GridView  from './js/GridView';
import _GlobalEventEmitter  from './js/AppGlobalEventEmitter';
import _AppDeviceInfo  from './js/AppDeviceInfo';
import _UserLogin from './js/UserLogin';
import _ToastManager from './js/ToastManager';
import _ImageKit from './js/ImageKit';
import ImageKitClass from "react-native-yuanxinkit/js/ImageKit";

export const Pickers = _YuanXinPicker;
export const GridView = _GridView;
export const GlobalEventEmitter = _GlobalEventEmitter; //应用程序事件监听
export const AppDeviceInfo = _AppDeviceInfo; //应用设备相关信息
export const UserLogin =_UserLogin;
export const ToastManager  = _ToastManager;
export const ImageKit  = _ImageKit;
