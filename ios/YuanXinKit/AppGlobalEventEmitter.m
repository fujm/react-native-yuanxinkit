//
//  AppGlobalEventEmitter.m
//  YuanXinKit
//
//  Created by 晏德智 on 2016/10/26.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import "AppGlobalEventEmitter.h"
#import <Security/Security.h>

#if __has_include(<React/RCTBridge.h>)
#import <React/RCTBridge.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTConvert.h>
#else
#import <React/RCTConvert.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTBridge.h>
#endif

@interface AppGlobalEventEmitter ()

- (void)bridgeNotification:(NSNotification *)notification;

@end

@implementation AppGlobalEventEmitter

//@synthesize bridge = _bridge;

RCT_EXPORT_MODULE()

#if __has_include(<React/RCTEventEmitter.h>)
- (void)startObserving
{

}

- (void)stopObserving
{

}

- (NSArray<NSString *> *)supportedEvents{
    return @[ @"onNotification"];
}
#endif

RCT_EXPORT_METHOD(addObserver:(NSString *)notificationName)
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(bridgeNotification:)
                                                 name:notificationName
                                               object:nil];
}

RCT_EXPORT_METHOD(postNotification:(NSString *)notificationName userInfo:(NSDictionary *)userInfo)
{
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName
                                                        object:nil
                                                      userInfo:userInfo];
}

RCT_EXPORT_METHOD(removeObserver:(NSString *)notificationName)
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:notificationName
                                                  object:nil];
}

- (void)bridgeNotification:(NSNotification *)notification
{
    
#if __has_include(<React/RCTEventEmitter.h>)
    [self sendEventWithName:@"onNotification" body:@{
                                                     @"name": notification.name,
                                                     @"userInfo": notification.userInfo ?: [NSNull null],
                                                     }];
#else
    [self.bridge.eventDispatcher sendDeviceEventWithName:@"onNotification"
                                                    body:@{
                                                           @"name": notification.name,
                                                           @"userInfo": notification.userInfo ?: [NSNull null],
                                                           }];
#endif
    
   
}

- (NSDictionary *)constantsToExport
{
    return @{
             @"UIApplicationNotifications": @{
                     @"UIApplicationDidEnterBackgroundNotification": UIApplicationDidEnterBackgroundNotification,
                     @"UIApplicationWillEnterForegroundNotification": UIApplicationWillEnterForegroundNotification,
                     @"UIApplicationDidFinishLaunchingNotification": UIApplicationDidFinishLaunchingNotification,
                     @"UIApplicationDidBecomeActiveNotification": UIApplicationDidBecomeActiveNotification,
                     @"UIApplicationWillResignActiveNotification": UIApplicationWillResignActiveNotification,
                     @"UIApplicationDidReceiveMemoryWarningNotification": UIApplicationDidReceiveMemoryWarningNotification,
                     @"UIApplicationWillTerminateNotification": UIApplicationWillTerminateNotification,
                     @"UIApplicationSignificantTimeChangeNotification": UIApplicationSignificantTimeChangeNotification,
                     @"UIApplicationWillChangeStatusBarOrientationNotification": UIApplicationWillChangeStatusBarOrientationNotification,
                     @"UIApplicationDidChangeStatusBarOrientationNotification": UIApplicationDidChangeStatusBarOrientationNotification,
                     @"UIApplicationStatusBarOrientationUserInfoKey": UIApplicationStatusBarOrientationUserInfoKey,
                     @"UIApplicationWillChangeStatusBarFrameNotification": UIApplicationWillChangeStatusBarFrameNotification,
                     @"UIApplicationDidChangeStatusBarFrameNotification": UIApplicationDidChangeStatusBarFrameNotification,
                     },
             @"UIWindowNotifications": @{
                     @"UIWindowDidBecomeVisibleNotification": UIWindowDidBecomeVisibleNotification,
                     @"UIWindowDidBecomeHiddenNotification": UIWindowDidBecomeHiddenNotification,
                     @"UIWindowDidBecomeKeyNotification": UIWindowDidBecomeKeyNotification,
                     @"UIWindowDidResignKeyNotification": UIWindowDidResignKeyNotification,
                     },
             @"UIKeyboardNotifications": @{
                     @"UIKeyboardWillShowNotification": UIKeyboardWillShowNotification,
                     @"UIKeyboardDidShowNotification": UIKeyboardDidShowNotification,
                     @"UIKeyboardWillHideNotification": UIKeyboardWillHideNotification,
                     @"UIKeyboardDidHideNotification": UIKeyboardDidHideNotification,
                     @"UIKeyboardWillChangeFrameNotification": UIKeyboardWillChangeFrameNotification,
                     @"UIKeyboardDidChangeFrameNotification": UIKeyboardDidChangeFrameNotification,
                     },
             };
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
