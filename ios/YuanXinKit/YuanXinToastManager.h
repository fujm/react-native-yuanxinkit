//
//  YuanXinToastManager.h
//  YuanXinKit
//
//  Created by 晏德智 on 2017/4/11.
//  Copyright © 2017年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>
#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#import <React/RCTBridge.h>
#import <React/RCTImageSource.h>
#import <React/RCTImageLoader.h>
#import <React/RCTConvert.h>
#import <React/RCTFont.h>
#import <React/RCTUtils.h>
#import <React/RCTLog.h>
#else
#import <React/RCTBridgeModule.h>
#import <React/RCTBridge.h>
#import <React/RCTImageSource.h>
#import <React/RCTImageLoader.h>
#import <React/RCTConvert.h>
#import <React/RCTFont.h>
#import <React/RCTUtils.h>
#import <React/RCTLog.h>
#endif

@interface YuanXinToastManager : NSObject<RCTBridgeModule>

@end
