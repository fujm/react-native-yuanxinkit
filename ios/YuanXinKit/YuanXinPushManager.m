//
//  YuanXinPushManager.m
//  YuanXinKit
//
//  Created by wangxue on 16/10/13.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import "YuanXinPushManager.h"
#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTConvert.h>
#import <React/RCTBridge.h>
#import <React/RCTUtils.h>
#else
#import <React/RCTConvert.h>
#import <React/RCTBridge.h>
#import <React/RCTUtils.h>
#endif
#import "AppDeviceInfoManager.h"
//#import "YuaXinHTTPSessionManager.h"

@interface YuanXinPushManager ()

@end
@implementation YuanXinPushManager
@synthesize bridge = _bridge;

RCT_EXPORT_MODULE();

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

- (instancetype)init{
    self = [super init];
    if(self){
        
    }
    return self;
}


//改变app角标
RCT_EXPORT_METHOD(changeAppBadgeWithCount:(NSString *)count resolver:(RCTPromiseResolveBlock)resolve){
    
    NSString *countstr = [RCTConvert NSString:count];
    if ([countstr rangeOfString:@"+"].location != NSNotFound) {
        
        NSInteger Addcount = [countstr substringFromIndex:1].integerValue;
        NSInteger badge = RCTSharedApplication().applicationIconBadgeNumber;
        [RCTSharedApplication() setApplicationIconBadgeNumber:badge+Addcount];
        
    }else if ([countstr rangeOfString:@"-"].location != NSNotFound) {
        NSInteger subcount = [countstr substringFromIndex:1].integerValue;
        NSInteger badge = RCTSharedApplication().applicationIconBadgeNumber;
        [RCTSharedApplication() setApplicationIconBadgeNumber:badge-subcount];
    }else {
        
    }
}

+ (NSString *)isEmpty:(NSString *)string {
    if(string == nil || string == NULL){
        return @"0";
    }else{
        return [NSString stringWithFormat:@"%@",string];
    }
}



@end
