//
//  UIColor+HexString.h
//  YuanXinKit
//
//  Created by 晏德智 on 2017/4/17.
//  Copyright © 2017年 晏德智. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexString)

+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
