//
//  NSString+Encrypto.h
//  YuanXinKit
//
//  Created by 晏德智 on 16/10/14.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Encrypto)

- (NSString *) md5;

/*
- (NSString *) sha1_base64;
- (NSString *) md5_base64;
- (NSString *) base64;
 */

- (NSString *) sha1;
- (NSString *) sha224;
- (NSString *) sha256;
- (NSString *) sha384;
- (NSString *) sha512;

@end
