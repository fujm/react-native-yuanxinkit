//
//  ImageKit.h
//  YuanXinKit
//
//  Created by GJS on 2019/8/19.
//  Copyright © 2019 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#else
#import <React/RCTBridgeModule.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface ImageKit : NSObject <RCTBridgeModule>

@end

NS_ASSUME_NONNULL_END
