//
//  AppGlobalEventEmitter.h
//  YuanXinKit
//
//  Created by 晏德智 on 2016/10/26.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#else
#import <React/RCTBridgeModule.h>
#endif

#if __has_include(<React/RCTEventEmitter.h>)
#import <React/RCTEventEmitter.h>

@interface AppGlobalEventEmitter : RCTEventEmitter

#else

@interface AppGlobalEventEmitter : NSObject<RCTBridgeModule>
#endif

@end
