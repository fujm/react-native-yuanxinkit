//
//  YuanXinKeyChainManager.h
//  YuanXinKit
//
//  Created by wangxue on 16/10/11.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#import <React/RCTConvert.h>
#import <React/RCTBridge.h>
#import <React/RCTUtils.h>
#import <React/RCTLog.h>
#else
#import <React/RCTBridgeModule.h>
#import <React/RCTConvert.h>
#import <React/RCTBridge.h>
#import <React/RCTUtils.h>
#import <React/RCTLog.h>
#endif

@interface YuanXinKeyChainManager : NSObject<RCTBridgeModule>

//存
+ (BOOL)savefromService:(NSString *)userName Data:(id)data;

//查
+ (id)keyChainQuery;

//删
+ (BOOL)resetKeyChain;
@end
