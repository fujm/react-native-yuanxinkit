//
//  LoginService.h
//  YuanXinKit
//
//  Created by 晏德智 on 2016/11/23.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserService: NSObject

@property (nonatomic, strong) NSString *loginAddress;

+ (instancetype)shareInstance;

//登陆
+ (void)loginParams:(NSDictionary *)paramDict
          success:(void (^)(NSDictionary *data))success
          failure:(void (^)(NSError *error))failure;
//退出登陆
+ (void)loginOut:(void(^)(id success))success
         failure:(void(^)(NSError *error))failure;
//刷新token
+ (void)refreshToken:(void(^)(NSDictionary *data))success
             failure:(void(^)(NSError *error))failure;
//
+ (void)IMLogin:(void (^)(NSDictionary *data))success
        failure:(void (^)(NSError *error))failure;

+ (NSString *)getOAuthToken;

@end
