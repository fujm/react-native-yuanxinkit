//
//  YuanXinPushManager.h
//  YuanXinKit
//
//  Created by wangxue on 16/10/13.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#else
#import <React/RCTBridgeModule.h>
#endif

@interface YuanXinPushManager : NSObject<RCTBridgeModule>

@end
