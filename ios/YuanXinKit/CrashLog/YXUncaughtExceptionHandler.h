//
//  UncaughtExceptionHandler.h
//  YuanXinKit
//
//  Created by 晏德智 on 2017/3/28.
//  Copyright © 2017年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YXUncaughtExceptionHandler:NSObject

@end

void YXInstallUncaughtExceptionHandler(void);
