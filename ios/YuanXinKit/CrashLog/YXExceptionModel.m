//
//  ExceptionModel.m
//  YuanXinKit
//
//  Created by 晏德智 on 2017/3/28.
//  Copyright © 2017年 晏德智. All rights reserved.
//

#import "YXExceptionModel.h"

NSString * const YXUncaughtExceptionHandlerSignalExceptionName = @"UncaughtExceptionHandlerSignalExceptionName";
NSString * const YXUncaughtExceptionHandlerSignalKey = @"UncaughtExceptionHandlerSignalKey";
NSString * const YXUncaughtExceptionHandlerAddressesKey = @"UncaughtExceptionHandlerAddressesKey";

volatile const int32_t YXUncaughtExceptionCount = 0;
const int32_t YXUncaughtExceptionMaximum = 10;

const NSInteger YXUncaughtExceptionHandlerSkipAddressCount = 4;
const NSInteger YXUncaughtExceptionHandlerReportAddressCount = 5;

@implementation YXExceptionModel

@end
