//
//  ExceptionModel.h
//  YuanXinKit
//
//  Created by 晏德智 on 2017/3/28.
//  Copyright © 2017年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const YXUncaughtExceptionHandlerSignalExceptionName;
extern NSString * const YXUncaughtExceptionHandlerSignalKey;
extern NSString * const YXUncaughtExceptionHandlerAddressesKey;

extern const volatile int32_t YXUncaughtExceptionCount;
extern const int32_t YXUncaughtExceptionMaximum;

extern const NSInteger YXUncaughtExceptionHandlerSkipAddressCount;
extern const NSInteger YXUncaughtExceptionHandlerReportAddressCount;

@interface YXExceptionModel : NSObject

@end
