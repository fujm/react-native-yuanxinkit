//
//  GlobalDeviceInfo.h
//  YuanXinKit
//
//  Created by 晏德智 on 2016/10/27.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalDeviceInfo : NSObject

//远薪移动唯一标识
@property (nonatomic,strong)          NSString     * appid;

@property (nonatomic,strong,readonly) NSString     * machineName;

@property (nonatomic,strong,readonly) NSString     * systemName;

@property (nonatomic,strong,readonly) NSString     * systemVersion;

@property (nonatomic,strong,readonly) NSString     * bundleId;

@property (nonatomic,strong,readonly) NSString     * appVersion;

@property (nonatomic,strong,readonly) NSString     * buildNumber;

@property (nonatomic,strong,readonly) NSString     * model;

//设备唯一ID
@property (nonatomic,strong,readonly) NSString     * uniqueId;

//设备名称
@property (nonatomic,strong,readonly) NSString     * deviceName;

@property (nonatomic,strong,readonly) NSString     * deviceLocale;

@property (nonatomic,strong,readonly) NSString     * timezone;

@property (nonatomic,strong,readonly) NSString     * userAgent;

@property (nonatomic,strong,readonly) NSString     * deviceCountry;

@property (nonatomic,assign,readonly) BOOL           isEmulator;


+(instancetype)shareInstance;

@end
