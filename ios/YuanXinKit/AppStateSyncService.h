//
//  AppStateSyncManager.h
//  YuanXinKit
//
//  Created by 晏德智 on 2016/10/26.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppStateSyncService : NSObject

//+(instancetype)shareInstance;

/**
 * 收集客户端信息
 */
+ (void)sendClientInfoSuccessBlock:(void (^)(id responseObject))successBlock
                        errorBlock:(void (^)(NSError *error))errorBlock;

/**
 * (NSData *)deviceToken 转成字符串
 */
+ (NSString *)deviceTokenToString:(NSData *)deviceToken;

/**
 * Note: 已弃用
 * 将推送消息的Token同步到Server
 */
+ (void)sendDeviceTokenToServer:(NSData *)deviceToken;

/**
 * 将 pushToken 存储在本地
 */
+ (void)saveDeviceToken:(NSData *)deviceToken;

/**
 * 取存储在本地的 pushToken
 */
+ (NSString *)getSavedDeviceToken;

/**
 * 将推送消息的 pushToken 同步到Server
 */
+ (void)bindingDeviceTokenToServer:(NSString *)deviceToken;

/**
 * 将推送消息的 pushToken 同步到Server
 */
+ (void)bindingDeviceTokenToServer;

/**
 * 将用户与设备解绑
 */
+ (void)unbindingDeviceTokenToServer:(NSString *)token;

/**
 *  将Voip 推送的Token送到服务端
 */
+ (void)sendDeviceVoipTokenToServer:(NSData *)deviceToken;

/**
 * 进入前台与后台通知Server
 */
+ (void)changeApplicationState:(int)state
                  successBlock:(void (^)(id responseObject))successBlock
                    errorBlock:(void (^)(NSError *error))errorBlock;

/**
 * 进入前后返回结果处理
 */
+ (void)changeApplicationStateSuccessResult:(id)responseObject;


@end
