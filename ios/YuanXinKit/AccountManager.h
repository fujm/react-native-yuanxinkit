//
//  AccountService.h
//  YuanXinKit
//
//  Created by 晏德智 on 2016/11/23.
//  Copyright © 2016年 晏德智. All rights reserved.
//
#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#import <React/RCTBridge.h>
#import <React/RCTConvert.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTUtils.h>
#import <React/RCTEventDispatcher.h>
#else
#import <React/RCTBridgeModule.h>
#import <React/RCTBridge.h>
#import <React/RCTConvert.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTUtils.h>
#import <React/RCTEventDispatcher.h>
#endif

#if __has_include(<React/RCTEventEmitter.h>)
#import <React/RCTEventEmitter.h>

@interface AccountManager : RCTEventEmitter
#else
@interface AccountManager : NSObject<RCTBridgeModule>
#endif


@end
