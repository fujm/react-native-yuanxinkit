//
//  AppBehaviorStatistic.h
//  YuanXinKit
//
//  Created by 晏德智 on 2017/3/27.
//  Copyright © 2017年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 用户行为统计
 */
@interface AppBehaviorStatistic : NSObject

+ (instancetype)shareInstance;

//启动
- (void)startObserving;

//停止通知
- (void)stopObserving;

//点击消息ID 打开APP
+ (void)openClickPushMesssage:(NSDictionary *)notificationInfo;

//将已收到的消息ID 回发给服务器，表示已到达。
+ (void)syncMessageState:(NSArray *)messageIDS;

@end
