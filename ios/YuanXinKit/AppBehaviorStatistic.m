//
//  AppBehaviorStatistic.m
//  YuanXinKit
//
//  Created by 晏德智 on 2017/3/27.
//  Copyright © 2017年 晏德智. All rights reserved.
//

#import "AppBehaviorStatistic.h"
#import "NetworkingManager.h"

NSString *const YuanXinApplicationIconBadgeNumberSettings = @"ApplicationIconBadgeNumberSettings";

@interface AppBehaviorStatistic(private)

@end

@implementation AppBehaviorStatistic

+ (instancetype)shareInstance{
    static AppBehaviorStatistic *_appBehaviorStatistic = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _appBehaviorStatistic = [[self alloc] init];
    });
    
    return _appBehaviorStatistic;
}

- (void)startObserving
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeApplicationIconBadgeNumber:)
                                                 name:YuanXinApplicationIconBadgeNumberSettings
                                               object:nil];
}

- (void)stopObserving{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)dealloc{
    [self stopObserving];
}

- (void)changeApplicationIconBadgeNumber:(NSDictionary *)data{
    
    [[NetworkingManager shareInstance] POST:@"NewPushService/badge/changeBadge"
                                 parameters:data
                                    success:^(NSURLSessionDataTask *task, id responseObject) {
                                        
                                    } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                        
                                    }];

}


+ (void)openClickPushMesssage:(NSDictionary *)notificationInfo{
    NSString *messageID = [notificationInfo objectForKey:@"un"];
    if(messageID){
        NSDictionary *data= @{@"MessageId":messageID};
        [[NetworkingManager shareInstance] POST:@"NewPushService/messageStatistics/reading"
                                     parameters:data
                                        success:^(NSURLSessionDataTask *task, id responseObject) {
                                            
                                        } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                            
                                        }];
    }
}

+ (void)syncMessageState:(NSArray *)messageIDS{
    
    NSDictionary *data= @{@"MessageId":messageIDS};
    [[NetworkingManager shareInstance] POST:@"NewPushService/messageStatistics/successSend"
                                 parameters:data
                                    success:^(NSURLSessionDataTask *task, id responseObject) {
                                        
                                    } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                        
                                    }];
    
}

@end
