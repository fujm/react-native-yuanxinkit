//
//  YuanXinDeviceInfoManager.m
//  YuanXinKit
//
//  Created by wangxue on 16/10/10.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import "AppDeviceInfoManager.h"
#import "DeviceUID.h"
#import <UIKit/UIKit.h>
#import "NSString+Encrypto.h"
#import "GlobalDeviceInfo.h"

@interface AppDeviceInfoManager()

@end

@implementation AppDeviceInfoManager

RCT_EXPORT_MODULE()

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

- (NSDictionary *)constantsToExport
{
   // UIDevice *currentDevice = [UIDevice currentDevice];
    
    return @{
             @"systemName": [GlobalDeviceInfo shareInstance].systemName,
             @"systemVersion":  [GlobalDeviceInfo shareInstance].systemVersion,
             @"model": [GlobalDeviceInfo shareInstance].model,
             @"brand": @"Apple",
             @"deviceId": [GlobalDeviceInfo shareInstance].uniqueId,
             @"deviceName":  [GlobalDeviceInfo shareInstance].deviceName,
             @"deviceLocale": [GlobalDeviceInfo shareInstance].deviceLocale,
             @"deviceCountry": [GlobalDeviceInfo shareInstance].deviceCountry ?: [NSNull null],
             @"uniqueId": [GlobalDeviceInfo shareInstance].uniqueId,
             @"bundleId": [GlobalDeviceInfo shareInstance].bundleId,
             @"appVersion":[GlobalDeviceInfo shareInstance].appVersion,
             @"buildNumber":[GlobalDeviceInfo shareInstance].buildNumber,
             @"systemManufacturer": @"Apple",
             @"userAgent": [GlobalDeviceInfo shareInstance].userAgent,
             @"timezone": [GlobalDeviceInfo shareInstance].timezone,
             @"isEmulator": @([GlobalDeviceInfo shareInstance].isEmulator),
             };
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getDarkMode) {
    NSString *mode = @"light";
    if (@available(iOS 13.0, *)) {
        switch (UITraitCollection.currentTraitCollection.userInterfaceStyle) {
            case UIUserInterfaceStyleDark:
            mode = @"dark";
                break;
                
            default:
                mode = @"light";
                break;
        }
    } else {
        // Fallback on earlier versions
    }
    return mode;
}

@end
