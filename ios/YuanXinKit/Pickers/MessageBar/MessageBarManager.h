//
//  MessageBarManager.h
//  YuanXinKit
//
//  Created by 晏德智 on 2017/4/10.
//  Copyright © 2017年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageBarManager : NSObject


/**
 Sets the default options that CRToast will use when displaying a notification
 @param defaultOptions A dictionary of the options that are to be used as defaults for all subsequent
 showNotificationWithOptions:completionBlock and showNotificationWithMessage:completionBlock: calls
 */

+ (void)setDefaultOptions:(NSDictionary*)defaultOptions;

/**
 Queues a notification to be shown with a collection of options.
 @param options A dictionary of the options that are to be used when showing the notification, defaults
 will be used for all non present options. Options passed in will override defaults
 @param completion A completion block to be fired at the completion of the dismisall of the notification
 @param appearance A  block to be fired when the notification is actually shown -- notifications can queue,
 and this block will only fire when the notification actually becomes visible to the user. Useful for
 synchronizing sound / vibration.
 */

+ (void)showNotificationWithOptions:(NSDictionary*)options apperanceBlock:(void (^)(void))appearance completionBlock:(void (^)(void))completion;

/**
 Queues a notification to be shown with a collection of options.
 @param options A dictionary of the options that are to be used when showing the notification, defaults
 will be used for all non present options. Options passed in will override defaults
 @param completion A completion block to be fired at the completion of the dismisall of the notification
 */

+ (void)showNotificationWithOptions:(NSDictionary*)options completionBlock:(void (^)(void))completion;

/**
 Queues a notification to be shown with a given message
 @param message The notification message to be shown. Defaults will be used for all other notification
 properties
 @param completion A completion block to be fired at the completion of the dismisall of the notification
 */

+ (void)showNotificationWithMessage:(NSString*)message completionBlock:(void (^)(void))completion;

/**
 Immidiately begins the (un)animated dismisal of a notification
 */

+ (void)dismissNotification:(BOOL)animated;

/**
 * 关闭所有的通知消息
 */
+ (void)dismissAllNotifications:(BOOL)animated;

/**
 * 消除指定消息的通知 
 *
 */
+ (void)dismissAllNotificationsWithIdentifier:(NSString *)identifier animated:(BOOL)animated;

/**
 * 获取当前通知的队列
 */
+ (NSArray *)notificationIdentifiersInQueue;

/**
 * 检查是否有当前正在显示的通知
 */
+ (BOOL)isShowingNotification;


+ (instancetype)shareInstance;

@end
