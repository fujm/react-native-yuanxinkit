//
//  MessageBarView.h
//  YuanXinKit
//
//  Created by 晏德智 on 2017/4/10.
//  Copyright © 2017年 晏德智. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageBarDefaultOptions.h"

@interface MessageBarView : UIView

@property (nonatomic, weak)   MessageBarDefaultOptions *toast;

@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end
