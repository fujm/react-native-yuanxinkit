//
//  MessageBarViewController.h
//  YuanXinKit
//
//  Created by 晏德智 on 2017/4/10.
//  Copyright © 2017年 晏德智. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageBarView.h"

@interface MessageBarViewController : UIViewController

@property (nonatomic, assign) BOOL autorotate;
@property (nonatomic, weak) MessageBarDefaultOptions *notification;
@property (nonatomic, weak) UIView *toastView;
@property (nonatomic, assign) UIStatusBarStyle statusBarStyle;

@end
