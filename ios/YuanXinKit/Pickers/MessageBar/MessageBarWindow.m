//
//  MessageBarWindow.m
//  YuanXinKit
//
//  Created by 晏德智 on 2017/4/10.
//  Copyright © 2017年 晏德智. All rights reserved.
//

#import "MessageBarWindow.h"

@implementation MessageBarWindow

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent*)event {
    for (UIView *subview in self.subviews) {
        if ([subview hitTest:[self convertPoint:point toView:subview] withEvent:event] != nil) {
            return YES;
        }
    }
    return NO;
}

@end
