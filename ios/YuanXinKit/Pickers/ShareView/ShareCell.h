//
//  ShareCell.h
//  ShareViewDemo
//
//  Created by wangxue on 16/9/20.
//  Copyright © 2016年 wangxue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareCell : UICollectionViewCell
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *title;
-(void)configCell:(NSDictionary *)dict;


@end
