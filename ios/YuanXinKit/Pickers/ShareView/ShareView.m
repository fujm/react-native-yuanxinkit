//
//  ShareView.m
//  ShareViewDemo
//
//  Created by wangxue on 16/9/20.
//  Copyright © 2016年 wangxue. All rights reserved.
//

#import "ShareView.h"
#import "ShareCell.h"
#define kWIDTH [UIScreen mainScreen].bounds.size.width
#define KHIGHT [UIScreen mainScreen].bounds.size.height
@interface ShareView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic) UIView *backView;

//显示几行(默认1行)
@property (nonatomic) NSUInteger maxLine;

//显示几列 （默认4列）
@property (nonatomic) NSUInteger maxColumn;



@property (nonatomic) NSArray *DataArry;


@end

@implementation ShareView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.theLengthToSide = 20;
    }
    
    return self;
}


- (void)hide
{
  
   [UIView animateWithDuration:0.4 animations:^{
        
        self.backView.alpha = 0;
        self.collectionView.alpha = 0;
        [self.collectionView removeFromSuperview];
    
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
   
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.DataArry[indexPath.row]] ;
    [dict removeObjectForKey:@"image"];
    if (self.selectblock) {
        
         self.selectblock([dict copy]);
    }
    [self hide];
}

- (void)configShareViewWithMaxLine:(NSUInteger)maxLine MaxColume:(NSUInteger)maxColum Direction:(ScrollDirection)drection DataArry:(NSArray *)dataArry
{
    self.maxLine = maxLine;
    self.maxColumn = maxColum;
    self.DataArry = dataArry;
    
    if (!maxColum)
    {
        self.maxColumn =4;
    }
    

    
    switch (drection) {
        case ScrollDirectionH:{
            self.flowLayout.scrollDirection =UICollectionViewScrollDirectionHorizontal;
            if (!self.maxLine)
            {
                if(self.DataArry.count < self.maxColumn*2)
                {
                    self.maxLine = 1;
                    
                }else{
                   
                    self.maxLine = 2;
                }
                    
            }else{
                
               if ( self.DataArry.count < self.maxLine*self.maxColumn)
               {
                  if((self.DataArry.count/self.maxLine)%self.maxColumn)
                  {
                    if (self.DataArry.count/self.maxLine > self.self.maxColumn)
                    {
                      self.maxLine = maxLine-1;
                    }else{
                        self.maxLine = self.DataArry.count/self.maxColumn;
                    }
                  }
               }
                
            }
            
            break;
            
        }
        case ScrollDirectionY:{
            self.flowLayout.scrollDirection =UICollectionViewScrollDirectionVertical;
            if (!self.maxLine)
                
            {
                if (self.DataArry.count/self.maxColumn && self.DataArry.count%self.maxColumn)
                {
                    self.maxLine = 2;
                }else{
                    
                    self.maxLine = 1;
                }
                
                
            }else if(((self.DataArry.count/self.maxColumn) <= self.maxLine-1))
            {
                if (self.DataArry.count/self.maxColumn)
                {
                    if (self.DataArry.count%self.maxColumn == 0)
                    {
                        self.maxLine = self.DataArry.count/self.maxColumn;
                        
                    }else{
                        
                        self.maxLine = self.DataArry.count/self.maxColumn+1;
                    }
                    
                }else
                {
                    self.maxLine = 1;
                }
                
            }
            

            break;
            
        }
            
            
        default:
            break;
    }

    
    [self addSubview:self.backView];

    [self.collectionView reloadData];
   
  
}


#pragma mark - 懒加载



- (UIView *)backView

{
    if (!_backView)
        
    {
        _backView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backView.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.58];
        UITapGestureRecognizer *tapGest = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
        [_backView addGestureRecognizer:tapGest];
        
        
    }
    
    return _backView;
}

#pragma mark -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.DataArry.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ShareCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    [cell configCell:self.DataArry[indexPath.row]];
    
    return cell;
}

- (CGFloat)getCollectionViewHeight
{
    CGFloat Height = ( 100 * self.maxLine )+ 20+10*(self.maxLine-1);
    
    
    return Height;
}

- (CGFloat)getMiddleCollectionViewPointY
{
    CGFloat pointY = (KHIGHT/2) - ([self getCollectionViewHeight]/2);
    
    return pointY;
}

#pragma mark - 懒加载

- (UICollectionView *)collectionView{
    
    if (!_collectionView)
        
    {
        CGFloat Y ;
        CGFloat x;
        CGFloat width;
        if (self.contentModel)
        {
            Y = [self getMiddleCollectionViewPointY];
            x = self.theLengthToSide;
            width = kWIDTH-(self.theLengthToSide*2);
        }else{
            Y = KHIGHT - [self getCollectionViewHeight];
            x = 0;
            width = kWIDTH;
        }
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(x,Y, width, [self getCollectionViewHeight]) collectionViewLayout:self.flowLayout];
        [self addSubview:_collectionView];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        
        _collectionView.backgroundColor = [UIColor whiteColor];
        
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        [_collectionView registerClass:[ShareCell class] forCellWithReuseIdentifier:@"Cell"];
        _collectionView.bounces = NO;
       
        _collectionView.layer.cornerRadius = self.viewCornerRadius;
        
        
    }
    
    return _collectionView;
    
}

- (UICollectionViewFlowLayout *)flowLayout{
    
    if (!_flowLayout)
    {
        CGFloat width;
        if (self.contentModel)
        {
        width = ((kWIDTH-(20+(self.theLengthToSide*2)))/self.maxColumn)-10;
            
        }else{
          width = ((kWIDTH-20)/self.maxColumn)-10;
        }
        _flowLayout = [[UICollectionViewFlowLayout alloc] init];
        
        _flowLayout.minimumLineSpacing = 10;
        _flowLayout.minimumInteritemSpacing = 10;
        
        _flowLayout.itemSize = CGSizeMake(width, 100);
        _flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    }
    
    return _flowLayout;
    
}

- (NSArray *)DataArry
{
    if (!_DataArry)
    {
        _DataArry = [NSArray new];
    }
    return _DataArry;
}


@end
