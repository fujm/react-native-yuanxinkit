//
//  ShareCell.m
//  ShareViewDemo
//
//  Created by wangxue on 16/9/20.
//  Copyright © 2016年 wangxue. All rights reserved.
//

#import "ShareCell.h"

@implementation ShareCell

#pragma mark - 生命周期

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame])
    {
        self.contentView.clipsToBounds = YES;
        [self imageView];
        [self title];
    }
    
    return self;
}

- (void)configCell:(NSDictionary *)dict
{
    self.imageView.image = dict[@"image"];
      self.title.text = dict[@"Title"];
}




#pragma mark - 懒加载

- (UIImageView *)imageView
{
    if (!_imageView)
        
    {
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeCenter;
        _imageView.clipsToBounds = YES;
        _imageView.frame = CGRectMake(0, 0, self.contentView.frame.size.width,  self.contentView.frame.size.height-20);
        [self.contentView addSubview:_imageView];
    }
    
    return _imageView;
    
}

- (UILabel *)title
{
    if (!_title)
        
    {
        _title = [[UILabel alloc] init];
        _title.frame = CGRectMake(0, self.contentView.frame.size.height-20, self.contentView.frame.size.width, 20);
        [self.contentView addSubview:_title];
        _title.font = [UIFont systemFontOfSize:11];
        _title.textAlignment = NSTextAlignmentCenter;
        _title.textColor = [UIColor lightGrayColor];
        
    }
    
    return _title;
}


@end
