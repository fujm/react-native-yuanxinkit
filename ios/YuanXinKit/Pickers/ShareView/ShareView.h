//
//  ShareView.h
//  ShareViewDemo
//
//  Created by wangxue on 16/9/20.
//  Copyright © 2016年 wangxue. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CompletBlock)(NSDictionary *dict);

typedef NS_ENUM(NSUInteger, ScrollDirection) {
    
    ScrollDirectionY,
    ScrollDirectionH,
};

typedef NS_ENUM(NSUInteger, contentModel) {
    contentModelBottom,
    contentModelMiddle,
};

@interface ShareView : UIView

@property (nonatomic, copy) CompletBlock selectblock;

@property (nonatomic) contentModel contentModel;

@property (nonatomic) CGFloat viewCornerRadius;


@property (nonatomic) CGFloat theLengthToSide;


- (void)configShareViewWithMaxLine:(NSUInteger )maxLine MaxColume:(NSUInteger)maxColum Direction:(ScrollDirection)drection DataArry:(NSArray *)dataArry;

@end
