//
//  YuanXinKit.h
//  YuanXinKit
//
//  Created by 晏德智 on 16/9/20.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#else
#import <React/RCTBridgeModule.h>
#endif

@interface YuanXinPickerManager :NSObject<RCTBridgeModule>

@end
