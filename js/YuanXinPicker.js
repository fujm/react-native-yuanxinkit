import {
    NativeModules,
    Platform
}
from 'react-native'

const { YuanXinPickerManager  } = NativeModules;

const DEFAULT_SHARE_OPTIONS = {
    Items: [
        {
            Name: 'shareToQQ',
            Title: 'QQ',
            ImageSource: ''
        },
        {
            Name: 'shareToQzone',
            Title: 'QQ空间',
            ImageSource: ''
        },
        {
            Name: 'shareToTimeline',
            Title: '微信朋友圈',
            ImageSource: ''
        },
        {
            Name: 'shareToSession',
            Title: '微信好友',
            ImageSource: ''
        }],
    Layout: {
        maxLine: 2,
        maxColume: 4,
        scrollDirection: 1,
        cornerRadius:6,
        theLengthToSide:0,
        contentModel:0
    }
};

//弹框View
export default class YuanXinPicker {

    static sharePicker(options: Object): Promise {
        return YuanXinPickerManager.showShareView(Object.assign(DEFAULT_SHARE_OPTIONS, options));
    }
}