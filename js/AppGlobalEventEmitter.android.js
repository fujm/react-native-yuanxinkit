
import {
    NativeAppEventEmitter,
    NativeModules,
    Platform
} from 'react-native'

var GlobalEventEmitter = {
    addListener(eventName: String, callback: Function){

    },
    emit(eventName: String, data){

    },
    removeListener(eventName: String, callbackRef: Function){

    },
    removeAllListeners(eventName: String){

    },
    watchEnterBackground(callback: Function){

    },
    clearWatchEnterBackground(callbackRef: Function){

    },
    watchEnterEnterForeground(callback: Function){

    },
    clearWatchEnterEnterForeground(callbackRef: Function){

    }
};

GlobalEventEmitter.UIApplicationNotifications = {};
GlobalEventEmitter.UIWindowNotifications = {};
GlobalEventEmitter.UIKeyboardNotifications = {};

module.exports = GlobalEventEmitter;