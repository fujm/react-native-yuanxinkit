import {
    NativeAppEventEmitter,
    NativeModules,
    Platform
} from 'react-native'

const { AppDeviceInfoManager  } = NativeModules;

export default class AppDeviceInfo {

    //应用程序ID
    static get UniqueID() {
        delete AppDeviceInfo.UniqueID;
        return AppDeviceInfo.UniqueID = AppDeviceInfoManager.uniqueId;
    }

    static get InstanceId() {
        delete AppDeviceInfo.InstanceId;
        return AppDeviceInfo.InstanceId = AppDeviceInfoManager.instanceId;
    }

    static get DeviceId() {
        delete AppDeviceInfo.DeviceId;
        return AppDeviceInfo.DeviceId = AppDeviceInfoManager.deviceId;
    }

    static get SystemManufacturer() {
        delete AppDeviceInfo.SystemManufacturer;
        return AppDeviceInfo.SystemManufacturer = AppDeviceInfoManager.systemManufacturer;
    }

    static get Model() {
        delete AppDeviceInfo.Model;
        return AppDeviceInfo.Model = AppDeviceInfoManager.model;
    }

    static get Brand() {
        delete AppDeviceInfo.Brand;
        return AppDeviceInfo.Brand = AppDeviceInfoManager.brand;
    }

    static get SystemName() {
        delete AppDeviceInfo.SystemName;
        return AppDeviceInfo.SystemName = AppDeviceInfoManager.systemName;
    }

    static get SystemVersion() {
        delete AppDeviceInfo.SystemVersion;
        return AppDeviceInfo.SystemVersion = AppDeviceInfoManager.systemVersion;
    }

    static get BundleId() {
        delete AppDeviceInfo.BundleId;
        return AppDeviceInfo.BundleId = AppDeviceInfoManager.bundleId;
    }

    static get BuildNumber() {
        delete AppDeviceInfo.BuildNumber;
        return AppDeviceInfo.BuildNumber = AppDeviceInfoManager.buildNumber;
    }

    static get AppVersion() {
        delete AppDeviceInfo.AppVersion;
        return AppDeviceInfo.AppVersion = AppDeviceInfoManager.appVersion;
    }

    static get ReadableVersion() {
        delete AppDeviceInfo.ReadableVersion;
        return AppDeviceInfo.ReadableVersion = AppDeviceInfoManager.appVersion + "." + AppDeviceInfoManager.buildNumber;
    }

    static get DeviceName() {
        delete AppDeviceInfo.DeviceName;
        return AppDeviceInfo.DeviceName = AppDeviceInfoManager.deviceName;
    }

    static get UserAgent() {
        delete AppDeviceInfo.UserAgent;
        return AppDeviceInfo.UserAgent = AppDeviceInfoManager.userAgent;
    }

    static get DeviceLocale() {
        delete AppDeviceInfo.DeviceLocale;
        return AppDeviceInfo.DeviceLocale = AppDeviceInfoManager.deviceLocale;
    }

    static get DeviceCountry() {
        delete AppDeviceInfo.DeviceCountry;
        return AppDeviceInfo.DeviceCountry = AppDeviceInfoManager.deviceCountry;
    }

    static get Timezone() {
        delete AppDeviceInfo.Timezone;
        return AppDeviceInfo.Timezone = AppDeviceInfoManager.timezone;
    }

    static get IsEmulator() {
        delete AppDeviceInfo.IsEmulator;
        return AppDeviceInfo.IsEmulator = AppDeviceInfoManager.isEmulator;
    }

    static get SystemVersionInt() {
        delete AppDeviceInfo.systemVersionInt;
        return AppDeviceInfo.systemVersionInt = AppDeviceInfoManager.systemVersionInt;
    }

    static get DarkMode() {
        delete AppDeviceInfo.darkMode;
        return AppDeviceInfo.darkMode = AppDeviceInfoManager.getDarkMode();
    }
}

