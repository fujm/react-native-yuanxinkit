import {
    NativeModules,
    Platform
} from 'react-native'

const {
    YuanXinToastManager
} = NativeModules;

import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource';

const Default_NavigationBar_Options = {
    tagID : undefined,
    textAlignment: 1,//0 左对齐，1 居中 2 右对齐
    textColor:'#FFFFFF', //文本颜色
    textFont:{
        fontSize: 11.5,
        fontWeight: 'bold',
    },
    desTextAlignment:0,
    desTextColor:'#FFFFFF', //描述信息颜色
    desTextFont:{
        fontSize: 11.5,
        fontWeight: 'normal',
    },
    timeInterval: 1.0, //显示时间
    padding: 2, //边距
    showActivityIndicator : false, //是否显示进度
    activityIndicatorAlignment : 1,//0 左对齐，1 居中 2 右对齐
    imageSource : undefined,
    imageAlignment : 1,//0 左对齐，1 居中 2 右对齐
    imageColor : '#F0F0F0',
    forceUserInteraction : false, //是否强制用户点击才会消息
    backgroundColor:'#3C99D9',
};

const Default_StatusBar_Options = {
    tagID : undefined,
    textAlignment: 1,//0 左对齐，1 居中 2 右对齐
    textColor:'#FFFFFF', //文本颜色
    textFont:{
        fontSize: 11.5,
        fontWeight: 'bold',
    },
    padding:0, //边距
    timeInterval: 1.0, //显示时间
    forceUserInteraction: false, //是否强制用户点击才会消息
    backgroundColor:'#38CC75',
};

//#38CC75  success
//#E64F42  error
//#3C99D9  info
//#FFA829  Warning

export default class ToastManager {

    static showNavigationBar(options : Object): Promise {
        if(options.imageSource){
           let source = resolveAssetSource(options.imageSource);
           options.imageSource = source;
        }
        return YuanXinToastManager.showNavigationBar({ ...Default_NavigationBar_Options, ...options});
    }

    static showStatusBar(options : Object): Promise {
        return YuanXinToastManager.showStatusBar({ ...Default_StatusBar_Options, ...options });
    }

    static showSuccessNotification(options : Object) :  Promise {
         return YuanXinToastManager.showNotifications({ ...Default_NavigationBar_Options, ...{ style : 'Success', backgroundColor:'#38CC75'}, ...options});
    }

    static showErrorNotification(options : Object) :  Promise {
         return YuanXinToastManager.showNotifications({ ...Default_NavigationBar_Options, ...{ style : 'Error', backgroundColor:'#E64F42'}, ...options});
    }

    static showWarningNotification(options : Object) :  Promise {
         return YuanXinToastManager.showNotifications({ ...Default_NavigationBar_Options, ...{ style : 'Warning', backgroundColor:'#FFA829'} , ...options});
    }

    static showInfoNotification(options : Object) :  Promise {
         return YuanXinToastManager.showNotifications({ ...Default_NavigationBar_Options, ...{ style : 'Info', backgroundColor:'#3C99D9'} , ...options});
    }
}