
import React, {
  Component,
} from 'react'
import {
  View,
  StyleSheet,
  Dimensions,
    ViewPropTypes
} from 'react-native'
import PropTypes from 'prop-types';

const { width: deviceWidth } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
  },
})

export default class GridView extends Component {

  static PropTypes = {
    rowWidth: PropTypes.number,
    columnCount: PropTypes.number.isRequired,
    dataSource: PropTypes.array.isRequired,
    renderCell: PropTypes.func.isRequired,
    style: ViewPropTypes.style,
  }

  // 构造
  constructor(props) {
    super(props)
    // 初始状态
    //this.state = {}

    this._columnWidth = (props.rowWidth || deviceWidth) / props.columnCount
  }

  render() {
    return (
      <View style={[this.props.style, styles.container, { width: this.props.rowWidth, }]}>
        {this._renderCells() }
      </View>
    )
  }

  _renderCells() {
    return this.props.dataSource.map((data, index, dataList) => {
      return (
        <View style={{ width: this._columnWidth, }} key={`cell-${(data.key != null) ? data.key : index}`}>
          {this.props.renderCell(data, index, dataList) }
        </View>
      )
    })
  }
}