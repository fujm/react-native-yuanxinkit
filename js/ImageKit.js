import {
    NativeModules,
    Platform
} from 'react-native'

const { ImageKit } = NativeModules;

export default class ImageKitClass {

    /**
     * 识别图中二维码
     * @param params
     * {
     *    filePath: '',  //图片路径
     * }
     * @returns {Promise}
     */
    static detectorQrCode(params: object): Promise {
        if(params.filePath)
            params.filePath = params.filePath.replace("file://", "");
        return ImageKit.detectorQrCode(params);
    }
}

