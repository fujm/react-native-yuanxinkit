/**
 * Created by 张文涛 on 2016/11/24.
 */
import {
    NativeEventEmitter,
    NativeModules,
    Platform
} from 'react-native'

const { AccountManager } = NativeModules;

const accountEmitter = new NativeEventEmitter(AccountManager);

const RefreshTokenChangeKey = 'OAuthRefreshTokenChange';
const LoginOutKey = 'OAuthLoginOutChnage';

let listeners = {};
let refreshTokenListener = null;
let loginOutListener = null;

function privateDisposeRefresTokenListener() {
    if(refreshTokenListener){
       refreshTokenListener.remove();
       refreshTokenListener = null;
    }
}

function privateInitRefresTokenListener(){
     privateDisposeRefresTokenListener();
     refreshTokenListener = accountEmitter.addListener('refreshToken', (data) => {
        let loginOutListeners = listeners[LoginOutKey];
        if (refreshTokenListeners && refreshTokenListeners.length > 0) {
            for (let i = 0, length = refreshTokenListeners.length; i < length; i++) {
                let watchFun = refreshTokenListeners[i];
                watchFun(data);
            }
        }
     });
}

function privateDisposeLoginOutListener() {
    if(loginOutListener){
       loginOutListener.remove();
       loginOutListener = null;
    }
}

function privateInitLoginOutListener(){
    privateDisposeLoginOutListener();

    loginOutListener = accountEmitter.addListener('loginOut', (data) => {
        let loginOutListeners = listeners[LoginOutKey];
        if (loginOutListeners && loginOutListeners.length > 0) {
            for (let i = 0, length = loginOutListeners.length; i < length; i++) {
                let watchFun = loginOutListeners[i];
                watchFun(data);
            }
        }
     });
}

export default class UserLogin {

    static userLogin(data): Promise {
        return AccountManager.UserLogin(data || {});
    }

    static refreshToken(): Promise {
        return AccountManager.RefreshToken();
    }

    static loginOut(): Promise {
        return AccountManager.loginOut();
    }

    //token发生变化时通知
    static addRefreshTokenChangeListener(fun: Function) {
        if (typeof fun === 'function') {
            if (!listeners[RefreshTokenChangeKey] || !listeners[RefreshTokenChangeKey].length) {
                listeners[RefreshTokenChangeKey] = [];
                privateInitRefresTokenListener();
            }
            listeners[RefreshTokenChangeKey].push(fun);
        }
    }

    //移除对Token变化时的临听
    static rmoveRefreshTokenChangeListener(fun: Function) {

        let rTokenListeners = listeners[RefreshTokenChangeKey];

        if (rTokenListeners && rTokenListeners.length > 0) {

            let i = rTokenListeners.indexOf(fun);
            if (i != -1) {
                rTokenListeners.splice(i, 1);
            }

            if (!rTokenListeners.length) {
                delete rTokenListeners[RefreshTokenChangeKey];
                privateDisposeRefresTokenListener();
            } else {
                rTokenListeners[RefreshTokenChangeKey] = rTokenListeners;
            }
        }
    }

    //token过期时退出登录
    static addLoginOutListener(fun: Function) {
        if (typeof fun === 'function') {
            if (!listeners[LoginOutKey] || !listeners[LoginOutKey].length) {
                listeners[LoginOutKey] = [];
                privateInitLoginOutListener();
            }
            listeners[LoginOutKey].push(fun);
        }
    }

    //移除token过期时退出登录
    static rmoveLoginOutListener(fun: Function) {

        let logingoutListeners = listeners[LoginOutKey];

        if (logingoutListeners && logingoutListeners.length > 0) {

            let i = logingoutListeners.indexOf(fun);
            if (i != -1) {
                logingoutListeners.splice(i, 1);
            }

            if (!logingoutListeners.length) {
                delete logingoutListeners[LoginOutKey];
                privateDisposeLoginOutListener();
            } else {
                logingoutListeners[LoginOutKey] = logingoutListeners;
            }
        }
    }
}
