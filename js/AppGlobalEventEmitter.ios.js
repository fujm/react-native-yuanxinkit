import {
    NativeEventEmitter,
    NativeModules,
    Platform
} from 'react-native'

const { AppGlobalEventEmitter } = NativeModules;

let _appGloblEmitter = new NativeEventEmitter(AppGlobalEventEmitter);
let listeners = {};

let notificationListener = _appGloblEmitter.addListener('onNotification', (data) => {
    let notifName = data.name;
    let callListeners = listeners[notifName];
    if (callListeners && callListeners.length > 0) {
        let notifData = data.userInfo;
        for (let i = 0, length = callListeners.length; i < length; i++) {
            let listener = callListeners[i];
            if (listener) {
                listener(notifData);
            }
        };
    }
});

/**
 * 添加一个原生事件
 */
function addListener(eventName: String, callback: Function) {

    let callListeners = listeners[eventName];
    if (!callListeners) {
        callListeners = [];
        AppGlobalEventEmitter.addObserver(eventName);
    }
    callListeners.push(callback);
    listeners[eventName] = callListeners;
};

/**
 * 触发一个原生的事件
 */
function emit(eventName: String, data: Object) {
    AppGlobalEventEmitter.postNotification(eventName, data);
};

function isEmpty(obj) {
    for (let prop in obj) {
        if (this.hasOwnProperty(prop))
            return false;
    }
    return true;
};

/**
 * 删除指定的监听
 */
function removeListener(eventName: String, callbackRef: Function) {

    let callListeners = listeners[eventName];
    if (callListeners && callListeners.length > 0) {
        let i = callListeners.indexOf(callbackRef);
        if (i != -1) {
            callListeners.splice(i, 1);
        }
    }

    if (callListeners.length == 0) {
        AppGlobalEventEmitter.removeObserver(eventName);
        delete listeners[eventName];

        if(isEmpty(listeners)){
            removeAllListeners(eventName);
        }
    }
}

/**
 * 删除指定事件的所有监听
 */
function removeAllListeners(eventName: String) {
    AppGlobalEventEmitter.removeObserver(eventName);
    if(notificationListener){
        notificationListener.remove();
    }
    if(_appGloblEmitter){
        _appGloblEmitter.removeCurrentListener();
    }
    delete listeners[eventName];
}

/**
 * 监听应用进入后台事件
 */
function watchEnterBackground(callback: Function) {
    addListener(AppGlobalEventEmitter.UIApplicationNotifications.UIApplicationDidEnterBackgroundNotification, callback);
}

/**
 * 清除APP进入后台事件
 */
function clearWatchEnterBackground(callbackRef: Function) {
    removeListener(AppGlobalEventEmitter.UIApplicationNotifications.UIApplicationDidEnterBackgroundNotification, callback);
}

/**
 * 监听应用进入前台的事件
 */
function watchEnterEnterForeground(callback: Function) {
    addListener(AppGlobalEventEmitter.UIApplicationNotifications.UIApplicationWillEnterForegroundNotification, callback);
}

function clearWatchEnterEnterForeground(callbackRef: Function) {
    removeListener(AppGlobalEventEmitter.UIApplicationNotifications.UIApplicationWillEnterForegroundNotification, callback);
}


/**
 * 监听软键盘将要弹出事件
 */
function watchKeyboardWillShow(callback: Function) {
    addListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardWillShowNotification, callback);
}

function clearWatchKeyboardWillShow(callbackRef: Function) {
    removeListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardWillShowNotification, callback);
}

/**
 * 监听软键盘已经弹出事件
 */
function watchKeyboardDidShow(callback: Function) {
    addListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardDidShowNotification, callback);
}

function clearWatchKeyboardDidShow(callbackRef: Function) {
    removeListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardDidShowNotification, callback);
}

/**
 * 监听软键盘将要隐藏事件
 */
function watchKeyboardWillHide(callback: Function) {
    addListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardWillHideNotification, callback);
}

function clearWatchKeyboardWillHide(callbackRef: Function) {
    removeListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardWillHideNotification, callback);
}

/**
 * 监听软键盘已经隐藏事件
 */
function watchKeyboardDidHide(callback: Function) {
    addListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardDidHideNotification, callback);
}

function clearWatchKeyboardDidHide(callbackRef: Function) {
    removeListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardDidHideNotification, callback);
}

/**
 * 键盘位置将要发生变化
 */
function watchKeyboardWillChangeFrame(callback: Function) {
    addListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardWillChangeFrameNotification, callback);
}

function clearWatchKeyboardWillChangeFrame(callbackRef: Function) {
    removeListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardWillChangeFrameNotification, callback);
}

/**
 * 键盘位置已经改变完成
 */
function watchKeyboardDidChangeFrame(callback: Function) {
    addListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardDidChangeFrameNotification, callback);
}

function clearWatchKeyboardDidChangeFrame(callbackRef: Function) {
    removeListener(AppGlobalEventEmitter.UIKeyboardNotifications.UIKeyboardDidChangeFrameNotification, callback);
}

/**
 * 
 */

var GlobalEventEmitter = {
    addListener,
    emit,
    removeListener,
    removeAllListeners,
    watchEnterBackground,
    clearWatchEnterBackground,
    watchEnterEnterForeground,
    clearWatchEnterEnterForeground,
    watchKeyboardWillChangeFrame,
    clearWatchKeyboardWillChangeFrame,
};

GlobalEventEmitter.UIApplicationNotifications = AppGlobalEventEmitter.UIApplicationNotifications;
GlobalEventEmitter.UIWindowNotifications = AppGlobalEventEmitter.UIWindowNotifications;
GlobalEventEmitter.UIKeyboardNotifications = AppGlobalEventEmitter.UIKeyboardNotifications;

module.exports = GlobalEventEmitter;